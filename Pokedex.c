#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//data structures
//--for each pokemon entry
typedef struct PokemonNode{
  char pokemonName[15];
  char pokemonType[10];
  char pokemonprimaryAbility[15];
  struct PokemonNode *next;
}PokemonNode;
//--for each player
typedef struct PlayerNode{
  char PlayerName[25];
  int pokemoncount;
  PokemonNode *PokemonArray[20];
  struct PokemonNode *next;
}PlayerNode;
//--for intital pokedex
typedef struct Pokedex{
  PokemonNode *pokemonHeadptr;
  PlayerNode *playerHeadptr;
}Pokedex; 
//-----------------------------------------------------------------------
//initilises the functions which will be under main
//----pokemon functions
PokemonNode* newPokemonNode(char name[25], char type[25], char primaryAbility[25]);
PokemonNode* findPokemon(Pokedex start, char name[25]);
void addPokemonToList(Pokedex *start, char name[25], char type[25], char ability[25]);
//----playerfunctions
PlayerNode* NewPlayerNode(char name[25]);
void AddPlayerToList(Pokedex *start,char name[25]);
PlayerNode* FindPlayer(Pokedex start,char name[25]);
void AddPokemonToPlayer(Pokedex start,char playername[25],char pokemonname[25]); 
//--pokedex functionality
void displayPokemonDetails(Pokedex start, char name[25]);
void DisplayPlayerDetails(Pokedex start, char name[25]);
void ListPokemon(Pokedex start);
void ListPlayers(Pokedex start);

//-----------------------------------------------------------------------
int main() {
  Pokedex pokedex;
  Pokedex *pokedexptr= &pokedex;
  PokemonNode *test;
  pokedex.pokemonHeadptr = NULL;
  pokedex.playerHeadptr = NULL;
  //populate pokemon
  addPokemonToList(pokedexptr,"Bulbasaur","Grass","Overgrow");
  addPokemonToList(pokedexptr,"Ivysaur","Grass","Overgrow");
  addPokemonToList(pokedexptr,"Venusaur","Grass","Overgrow");
  addPokemonToList(pokedexptr,"Arceus","Normal","Multitype");
  //populate players
  AddPlayerToList(pokedexptr,"aLiteralChild");
  AddPlayerToList(pokedexptr,"KidCaughtGod");
  AddPlayerToList(pokedexptr,"ChildA");
  AddPlayerToList(pokedexptr,"ChildB");
  printf("%s\n", pokedexptr->pokemonHeadptr->pokemonName);
  printf("%s\n", pokedexptr->playerHeadptr->PlayerName);
  displayPokemonDetails(pokedex, "Bulbasaur");
  ListPokemon(pokedex);
  ListPlayers(pokedex);
  AddPokemonToPlayer(pokedex, "ChildA", "Arceus");
  
  return 0;
}
//-----------------------------------------------------------------------
//Functions
//--creating new pokemon entry
PokemonNode* newPokemonNode(char name[25], char type[25], char ability[25]){
  PokemonNode *new_node = NULL;                // Create a pointer to a node structure, set it to NULL for safety
  new_node =malloc(sizeof(PokemonNode));       // Set aside space in memory for a node structure. If this fails new_node will be set to NULL

  if (new_node != NULL)                 // Always ensure the node exists before accessing it.
  {
    strcpy(new_node->pokemonName, name);
    strcpy(new_node->pokemonType, type);
    strcpy(new_node->pokemonprimaryAbility, ability);
    new_node->next=NULL;              // Set next (and any other pointers to NULL)
    
  } 
  return new_node;
}
//--adding new pokemon entry to list
void addPokemonToList(Pokedex *start, char name[25], char type[25], char ability[25]){

  PokemonNode *new_node = newPokemonNode(name, type, ability);
  PokemonNode *temp = start->pokemonHeadptr;

  // list is empty
  if (start->pokemonHeadptr==NULL)
  {
    start->pokemonHeadptr = new_node;
    return;
  }

  while (temp->next != NULL)   // If next is NULL we are on the last entry in the list (or we've broken our list!)
  {    
    temp = temp->next;         // Change our temp pointer from the current one to temp->next - "moving" us on to the next node
  } 
  temp->next = new_node;
  return;
}
//meant to return pointer to the node containing the pokemon of the desired name, or return null if not found, dont think it works properly but dont know how to fix
PokemonNode* findPokemon(Pokedex start, char name[25]){
  PokemonNode* temp = start.pokemonHeadptr;

  while (temp->next != NULL){
    
    if (*temp->pokemonName = name){
      return temp;
    }
    temp = temp->next;
  }
  return NULL;
}
//--creating new player entry
PlayerNode* NewPlayerNode(char name[25]){
  PlayerNode *new_node = NULL;                // Create a pointer to a node structure, set it to NULL for safety
  new_node =malloc(sizeof(PlayerNode));       // Set aside space in memory for a node structure. If this fails new_node will be set to NULL

  if (new_node != NULL)                 // Always ensure the node exists before accessing it.
  {
    strcpy(new_node->PlayerName, name);
    for (int i=0; i<20;i++){
      new_node->PokemonArray[i]=NULL;
    }
    new_node->next=NULL;              // Set next (and any other pointers to NULL)
    
  } 
  return new_node;
}
//--adding new player entry to list
void AddPlayerToList(Pokedex *start,char name[25]){
  PlayerNode *new_node = NewPlayerNode(name);
  PlayerNode *temp = start->playerHeadptr;

  // list is empty
  if (start->playerHeadptr==NULL)
  {
    start->playerHeadptr = new_node;
    return;
  }

  while (temp->next != NULL)   
  {    
    temp = temp->next;         
  } 
  temp->next = new_node;
  return;
}
//meant to return pointer to player if exists, else return NULL, not currently working as always returns null
PlayerNode* FindPlayer(Pokedex start,char name[25]){
  PlayerNode* temp = start.playerHeadptr;

  while (temp->next != NULL){
    
    if (*temp->PlayerName = name){
      return temp;
    }
    temp = temp->next;
  }
  return NULL;
  
}
//...doesnt add correct pokemon, sort tommorow with interface stuff
void AddPokemonToPlayer(Pokedex start,char playername[25],char pokemonname[25]){
  PlayerNode* player = FindPlayer(start, playername);
  PokemonNode* *pokemon = findPokemon(start, pokemonname);
  int i = 0;
  while (player->PokemonArray[i] !=NULL || i < 20){
    i++;
  }
  if(player->PokemonArray[i] == NULL){
    player->PokemonArray[i] = pokemon;
    player->pokemoncount++;
  }
  else if(i = 20){
    printf("You have collected the maximum amount of pokemon allowed.");
  }
  
}

void displayPokemonDetails(Pokedex start, char name[25]){
  PokemonNode* pokemon = findPokemon(start, name);
  
  printf("%s\n",pokemon->pokemonName);
  printf("%s\n",pokemon->pokemonType);
  printf("%s\n",pokemon->pokemonprimaryAbility);
}
//meant to display player details but doesnt work, sort later
void DisplayPlayerDetails(Pokedex start, char name[25]){
  PlayerNode* player = FindPlayer(start, name);
  int i = 0;
  printf("%s\n", player->PlayerName);
  while(player->PokemonArray[i] != NULL){
    printf("%s\n", player->PokemonArray[i]->pokemonName);
    i++;
  }
  printf("%s\n", player->pokemoncount);
}
void ListPokemon(Pokedex start){
  PokemonNode *temp = start.pokemonHeadptr;
  while (temp->next != NULL){
    printf("Pokemon: %s\n", temp->pokemonName);
    temp = temp->next;
  }
  printf("Pokemon: %s\n", temp->pokemonName);
  return;
}
void ListPlayers(Pokedex start){
  PlayerNode *temp = start.playerHeadptr;
  while (temp->next != NULL){
    printf("Player: %s\n", temp->PlayerName);
    temp = temp->next;
  }
  printf("Player: %s\n", temp->PlayerName);
  return;
}
